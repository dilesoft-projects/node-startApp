import {
  installModules, testAssertionFile, updateLib, updateModule, updateModules,
} from './functions';
import modules from '../server/config/modules.json';

async function update() {
  await updateLib('token-types');
  await updateModules(modules);
  process.exit();
}

update();

// testAssertionFile();
